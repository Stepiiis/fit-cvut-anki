# BI-MA2

## Vynechaná témata
 - důkaz integrace sudých, lichých a periodických funkcí ([5:20](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-04-vypocet-urciteho-integralu.pdf))
 - téma Numerická Integrace s výjimkou lichoběžníkového pravidla ([6](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-05-numericka-integrace.pdf))
 - důkaz věty odhadu součtu ([7:30](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-06-ciselne-rady.pdf))
 - důkaz věty o nejlepší aproximaci ([10:13](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-09-taylorova-veta.pdf))
 - důkaz věty o struktuře množiny řešení LRR ([11:24](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-10-linearni-rekurence.pdf))
 - Vandermondeovo lemma a jeho důkaz ([12:12](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-11-linearni-rekurence-s-konstantnimi-koeficienty.pdf))
 - důkaz věty o konstrukci báze řešení homogenní LRR s konstantními koeficienty pomocí charakteristických čísel násobnosti 1 ([12:13](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-11-linearni-rekurence-s-konstantnimi-koeficienty.pdf))
 - důkaz věty o konstrukci báze řešení homogenní LRR s konstantními koeficienty pomocí charakteristických čísel vyšší násobnosti ([12:16](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-11-linearni-rekurence-s-konstantnimi-koeficienty.pdf))
 - důkaz věty o konvergenci po složkách ([14:20](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-13-fce-vice-promennych.pdf))
 - důkaz věty o spojitosti vektorové funkce zadané pomocí spojitých funkcí jedné proměnné ([14:35](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-13-fce-vice-promennych.pdf))
 - důkaz věty o limitě složené vektorové funkce ([14:36](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-13-fce-vice-promennych.pdf))
 - důkaz Sylvestrova kritéria ([15:26](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-14-kvadraticke-formy.pdf))
 - důkaz postačující podmínky existence lokálního extrému funkce více proměnných ([16:24](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-15-extremy.pdf))
 - důkaz metody volby směru ve spádové metodě ([17:13](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-16-spadova-metoda.pdf))
